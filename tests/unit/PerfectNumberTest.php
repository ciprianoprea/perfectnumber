<?php

use PHPUnit\Framework\TestCase;
use ClassificationNumber\PerfectNumber;
use ClassificationNumber\Interfaces\ClassificationNumber;

class PerfectNumberTest extends TestCase
{

	public function testAssertPerfect()
	{
		$perfectNumber = (new PerfectNumber)->getClassification(6);

		$this->assertEquals('perfect',$perfectNumber);
	}

	public function testAssertAbundant()
	{
		$perfectNumber = (new PerfectNumber)->getClassification(12);

		$this->assertEquals('abundant',$perfectNumber);
	}

	public function testAssertDeficient()
	{
		$perfectNumber = (new PerfectNumber)->getClassification(15);

		$this->assertEquals('deficient',$perfectNumber);
	}

	public function testAssertNotPerfect()
	{
		$perfectNumber = (new PerfectNumber)->getClassification(20);

		$this->assertNotEquals('perfect',$perfectNumber);
	}

	public function testAssertInstance()
	{
		$perfectNumberInstance = (new PerfectNumber);

		$this->assertInstanceOf(ClassificationNumber::class,$perfectNumberInstance);
	}
}