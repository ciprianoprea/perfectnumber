<?php 

namespace ClassificationNumber;

use ClassificationNumber\Interfaces\ClassificationNumber;

/**
* 
*/
class PerfectNumber implements ClassificationNumber
{

	CONST PERFECT_NUMBER 	= 'perfect';
	CONST ABUNDANT_NUMBER 	= 'abundant';
	CONST DEFICIENT_NUMBER	= 'deficient';

	public function getClassification(int $number) : string
	{
		$this->validate($number);
		
		return $this->type($this->totalDividingNumbers($number),$number);
	}

	public function totalDividingNumbers(int $number) : int
	{
		$dividingNumbers = array_filter(range(1,$number),function($currentNumber) use ($number) {

			return $currentNumber < $number && $number%$currentNumber == 0;

		});

		return array_sum($dividingNumbers);
	}

	protected function validate($number){

		if($number <= 0)
		{
			throw new \Exception("Number must be positive", $number);
			
		}

	}

	public function type($total, $number)
	{
		if($total == $number)
		{
			return self::PERFECT_NUMBER;
		}

		if($total < $number)
		{
			return self::DEFICIENT_NUMBER;
		}

		return self::ABUNDANT_NUMBER;
	}
}