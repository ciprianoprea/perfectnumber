<?php  

namespace ClassificationNumber\Controllers;

use Api\ApiResponse;
use ClassificationNumber\Interfaces\ClassificationNumber;

class NumbersController
{

	public function getClassification($number,ClassificationNumber $classification) : string
	{
		try {

			return ApiResponse::successResponse([
				'number' => $number,
				'classification' => $classification->getClassification($number)
			]);
			
		} catch (\Exception $e) {
			
			return ApiResponse::setStatus(500)->errorResponse($e->getMessage());
		}
	}

}