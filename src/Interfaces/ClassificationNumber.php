<?php  

namespace ClassificationNumber\Interfaces;

interface ClassificationNumber
{
	public function getClassification(int $number);
}