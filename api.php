<?php  

require 'vendor/autoload.php';

use ClassificationNumber\Interfaces\ClassificationNumber;
use ClassificationNumber\PerfectNumber;
use ClassificationNumber\Controllers\NumbersController;
use Api\ApiResponse;
use Api\Auth;


if(isset($_REQUEST['action']))
{
	try {

		Auth::check();

	} catch (\Exception $e) {

		print ApiResponse::setStatus(400)->errorResponse($e->getMessage());
		return;
	}

	try {

		switch ($_REQUEST['action']) {
			case 'classification':
				$number = $_REQUEST['number'] ?? null;
				print (new NumbersController())->getClassification($number,new PerfectNumber);
			break;
			default:
				print ApiResponse::setStatus(404)->errorResponse('action error');
			break;
		}
		
	} catch (\Exception $e) {

		print ApiResponse::setStatus(500)->errorResponse($e->getMessage());
		
	}

	return;
}

print ApiResponse::setStatus(404)->errorResponse('you are in the root');