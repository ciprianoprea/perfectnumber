window._ = require('lodash');
window.Popper = require('popper.js').default;

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');

} catch (e) {}

window.Vue = require('vue');
window.axios = require('axios');



new Vue({
	el: '#classification',
	data : {
		number	 :'',
		status   :'',
		errorMessage   :''
	},

	methods:
	{
		check:function(obj){

			this.errorMessage = false;
			var data = new FormData();
			data.set('number', this.number);

			axios.post('api.php?action=classification&api_token=K6KDPOL33KDL3KLTJ00SK3403KDKS0',
				data,
				{ headers: {'Content-Type': 'multipart/form-data' }}
    		
    		).then((response => (this.status = response.data.data.classification)))
			.catch((error => {
			    this.errorMessage = error.response.data.message
			}));
		}
	}
})