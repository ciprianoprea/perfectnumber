<?php

namespace API;

class ApiResponse
{

	protected static $status = 200;

	public static function setStatus(int $statusCode)
	{
		$response = new static;
		
		$response::$status = $statusCode;

		return $response;
	}

	public static function successResponse($data)
	{

		$response =  [
			'data' => $data,
			'status' => self::$status
		];

		header('Content-Type: application/json');
		http_response_code(self::$status);

		return json_encode($response);
	}

	public static function errorResponse($message)
	{

		$response =  [
			'message' => $message,
			'status' => self::$status
		];

		header('Content-Type: application/json');
		http_response_code(self::$status);
		
		return json_encode($response);
	}


}